<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-certificate-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Certificate;

use Stringable;

/**
 * CertificateProviderInterface interface file.
 * 
 * This class represents a provider for certificate bundle files.
 * 
 * @author Anastaszor
 */
interface CertificateProviderInterface extends Stringable
{
	
	/**
	 * Gets the absolute full path of the file that has one or more valid
	 * certificates written in.
	 * 
	 * @return string
	 */
	public function getCertificateFilePath() : string;
	
}
